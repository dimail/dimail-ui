/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

define('fr.dinum/spam-filter/register', ['io.ox/core/extensions', 'io.ox/core/settings/util', 'gettext!io.ox/mail', 'settings!fr.dinum/spam-filter', 'io.ox/mail/mailfilter/settings/model'], function (ext, util, gt, settings, MailFilterModel) {
    'use strict';

    ext.point('io.ox/settings/security/settings/detail/mail').extend({
        id: 'spam',
        index: 150,
        render: function () {
            var nameRule = '[Paramètres sécurité] Déplacer les spams dans le dossier spam';
            this.$el.append(
                $('<fieldset>').addClass('spam').append(
                    $('<legend>').addClass('sectiontitle').append(
                        $('<h2>').text('Gestion des spams')
                    ),
                    $('<div>').addClass('checkbox custom small').append(
                        $('<label>').attr('for', 'spam-filter-toggle').append(
                            $('<input>')
                                .attr({
                                    type: 'checkbox',
                                    id: 'spam-filter-toggle',
                                    name: 'moveSpamToFolder',
                                    class: 'sr-only' // Accessible mais masqué visuellement
                                })
                                .prop('checked', settings.get('moveSpamToFolder', true))
                                .on('change', function () {

                                    settings.set('moveSpamToFolder', $(this).prop('checked')).save();
                                    if (settings.get('moveSpamToFolder')) {

                                        var newFilter = {
                                            test: {
                                                'comparison': 'contains',
                                                'headers': [
                                                    'Subject'
                                                ],
                                                'id': 'subject',
                                                'values': [
                                                    '*****SPAM*****'
                                                ]
                                            },
                                            actioncmds: [
                                                {
                                                    'id': 'move',
                                                    'into': 'default0/Pourriel'
                                                }
                                            ],
                                            rulename: nameRule
                                        };
                                        MailFilterModel.api.create(newFilter);
                                    } else {
                                        var ids = [];
                                        MailFilterModel.api.getRules()
                                        .then(function (data) {
                                            ids = data
                                            .filter(function (item) {
                                                return item.rulename === nameRule;
                                            })
                                            .map(function (item) {
                                                return item.id;
                                            });
                                            for (var i = 0; i < ids.length; i++) {
                                                var id = ids[i];
                                                MailFilterModel.api.deleteRule(id);
                                            }
                                        });
                                        for (var i = 0; i < ids.length; i++) {
                                            var id = ids[i];
                                            MailFilterModel.api.deleteRule(id);
                                        }
                                    }
                                }),
                            $('<i>').addClass('toggle').attr('aria-hidden', settings.get('moveSpamToFolder')),
                            'Déplacer les spams dans le dossier spam'
                        )
                    )
                )
            );
        }
    });
});
