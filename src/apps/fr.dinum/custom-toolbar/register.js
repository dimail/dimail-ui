/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

define('fr.dinum/custom-toolbar/register', [
    'io.ox/core/extensions', 'gettext!io.ox/core'], function (ext, gt) {
    'use strict';

    var toolbars = ext.point('io.ox/mail/toolbar/links');

    toolbars.replace({
        id: 'compose',
        index: 100,
        title: '+ Nouveau mail'
    });

    toolbars.replace({
        id: 'mark-read',
        prio: 'hi',
        mobile: 'lo',
        title: gt('Mark as read'),
        ref: 'io.ox/mail/actions/mark-read',
        section: 'flags',
        icon: 'fa-envelope-o'
    });

    toolbars.replace({
        id: 'mark-unread',
        prio: 'hi',
        icon: 'fa-envelope',
        mobile: 'lo',
        section: 'flags',
        title: gt('Mark as unread')
    });

    function assistance() {
        this.link('assistance', gt('Assistance'), function (e) {
            e.preventDefault();
            window.open('https://webmail.numerique.gouv.fr/assistance/', 'Assistance');
        });
    }

    ext.point('io.ox/core/appcontrol/right/help').extend({
        id: 'assistance',
        index: 600,
        extend: function () {
            assistance.apply(this, arguments);
        }
    });
});
