/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

define('fr.dinum/mail-list-paper-clip/register', [
    'io.ox/core/extensions',
    'io.ox/mail/api',
    'io.ox/backbone/settings',
    'io.ox/core/attachments/backbone',
    'io.ox/core/attachments/view',
    'io.ox/backbone/views/toolbar',
    'settings!io.ox/mail',
    'io.ox/backbone/views/actions/util'
], function (ext, api, settingsBackbone, AttachmentsBackbone, attachment, ToolbarView, settings, actionsUtil) {
    'use strict';

    var setAttachmentList = function () {
        var CustomAttachmentView;

        return function (baton) {
            if (baton.attachments.length === 0) return $.when();
            var $el = this;
            var list = baton.attachments.map(function (m) {
                m.group = 'mail';
                return m;
            });
            var collection = new attachment.Collection(list);
            var reuse = !!$el.data('view');
            var view = $el.data('view') || new attachment.List({
                AttachmentView: CustomAttachmentView,
                collection: collection,
                el: $el,
                mode: settings.get('attachments/layout/detail/' + _.display(), 'list')
            });
            view.openByDefault = true;

            view.$header.empty();
            var previewContainer = view.$el.find('.preview-container').first();
            if (previewContainer.length) {
                previewContainer.remove();
            }
            view.render();

            var listContainer = view.find('.mail-attachment-list').first();
            listContainer.removeClass('show-preview');

            var toolbarView = new ToolbarView({
                el: view.$header.find('.links')[0],
                inline: true,
                simple: true,
                dropdown: false,
                strict: false,
                point: 'io.ox/mail/attachment/links'
            });

            view.updateScrollControls = function (index) {
                if (index === undefined) index = this.getScrollIndex();
                var max = this.getMaxScrollIndex();
                this.$('.scroll-left').prop('disabled', index <= 0);
                this.$('.scroll-right').prop('disabled', index >= max);
            };

            view.renderInlineLinks = function () {
                var models = this.getValidModels();
                if (!models.length) return;
                toolbarView.setSelection(_(models).pluck('id'), { data: _(models).invoke('toJSON') });
            };

            view.listenTo(view.collection, 'add remove reset', function () {
                view.renderInlineLinks();
                view.updateScrollControls();
            });
            view.listenTo(baton.model, 'change:imipMail', view.renderInlineLinks);
            view.listenTo(baton.model, 'change:sharingMail', view.renderInlineLinks);

            view.$el.find('.scroll-right').prop('disabled', false);
            view.$el.find('.scroll-left').prop('disabled', false);

            if (!reuse) {
                view.$el.on('click', 'li.item', function (e) {
                    var node = $(e.currentTarget);
                    var clickTarget = $(e.target), id, data, baton;

                    if (clickTarget.hasClass('dropdown-toggle')) return;

                    id = node.attr('data-id');
                    data = collection.get(id).toJSON();
                    baton = ext.Baton({ simple: true, data: data, list: list, restoreFocus: clickTarget, openedBy: 'io.ox/mail/details' });
                    actionsUtil.invoke('io.ox/mail/attachment/actions/view', baton);
                });

                view.on('change:layout', function (mode) {
                    settings.set('attachments/layout/detail/' + _.display(), mode).save();
                });
            }

            var attachmentList = document.querySelector('.mail-attachment-list')[0];
            attachmentList.classList.remove('show-preview');

            view.$el.find('[role="toolbar"]').find('a[role="menuitem"]').attr('role', 'button');
            return view;
        };
    };

    ext.point('io.ox/mail/detail/attachments').extend({
        id: 'list-paper-clip',
        index: 300,
        draw: setAttachmentList()
    });
});
