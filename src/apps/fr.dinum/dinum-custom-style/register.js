/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

define('fr.dinum/dinum-custom-style/register', [
    'io.ox/core/extensions',
    'io.ox/backbone/views/extensible',
    'io.ox/core/settings/util',
    'gettext!io.ox/core',
    'settings!io.ox/core',
    'less!fr.dinum/dinum-custom-style/style',
    'less!fr.dinum/dinum-custom-style/lucky-style'], function (ext, ExtensibleView, util, gt, settings) {
    'use strict';

    function getTheme() {
        if (window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches) {
            return 'dark-mode';
        }
        return 'light';
    }

    // Applique le thème en fonction du paramètre choisi
    function applyTheme() {

        var theme = settings.get('design');
        if (theme === 'default') {
            theme = getTheme();
        }
        var htmlElement = document.documentElement;

        if (theme === 'dark-mode') {
            htmlElement.classList.add('dark-mode');
        } else {
            htmlElement.classList.remove('dark-mode');
            htmlElement.classList.add(theme);
        }
    }

    // Ajoute un lien sur le logo vers la boite de reception
    function linkToHomeOnlogo() {
        var div = document.getElementById('io-ox-top-logo');
        if (div) {
            div.role = 'menuitem';
            div.style.cursor = 'pointer';
            div.onclick = function () {
                window.location.href = '/appsuite/ui#!!&app=io.ox/mail&folder=default0/INBOX';
                window.location.reload();
            };
        }
    }

    if (document.getElementById('io-ox-top-logo')) {
        linkToHomeOnlogo();
        return;
    }

    var observer = new MutationObserver(function () {
        if (document.getElementById('io-ox-top-logo')) {
            linkToHomeOnlogo();
            observer.disconnect(); // Arrête d'observer une fois la div trouvée
        }
    });

    // Observe les changements du DOM
    observer.observe(document.body, { childList: true, subtree: true });

    applyTheme();

    // Change le thème si le paramètre change au niveau du navigateur
    window.matchMedia('(prefers-color-scheme: dark)').addEventListener('change', function () {
        applyTheme();
    });

    ext.point('io.ox/core/settings/detail/view/fieldset/first').replace({
        id: 'design',
        index: 400,
        render: function () {
        }
    });

    ext.point('io.ox/core/settings/detail/view/fieldset/first').replace({
        id: 'theme',
        index: 300,
        render: function (baton) {

            if (_.device('ie <= 11')) return;
            if (!settings.get('features/userDesigns', true)) return;
            // works only for default theme
            if (this.hasMoreThanOneTheme()) return;

            baton.$el.append(
                util.compactSelect('design', 'Thème actuel', this.model, getThemes(), { groups: true })
            );
            baton.$el.append('<span id="helpTheme" style="display:none;" class="help-block">Vous venez de sélectionner un autre thème, merci de recharger la page.</span>');
        }
    });

    settings.on('change:design', function () {
        $('#helpTheme').css('display', 'block');
    });


    // Modifie les options de thème par defaut d'OX
    function getThemes() {
        return [
            {
                label: 'En fonction du navigateur',
                options: [
                    { label: 'Automatique', value: 'default' }
                ]
            },
            {
                label: 'Standard',
                options: [
                    { label: 'Sombre', value: 'dark-mode' },
                    { label: 'Clair', value: 'light-mode' }
                    // { label: 'Lucky', value: 'lucky-mode' }
                ]
            }
        ];
    }
});
