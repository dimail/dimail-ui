/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

define('fr.dinum/create-event/register', ['io.ox/core/extensions', 'io.ox/core/folder/api', 'gettext!io.ox/core', 'settings!io.ox/calendar', 'io.ox/participants/add', 'less!fr.dinum/create-event/style'], function (ext, api, gt, settings, AddParticipantView) {
    'use strict';

    function addFolder(e) {
        e.preventDefault();
        ox.load(['io.ox/core/folder/actions/add']).done(function (add) {
            add(e.data.folder, { module: e.data.module });
        });
    }

    function shuffle(array) {
        var currentIndex = array.length, temporaryValue, randomIndex;

        while (currentIndex !== 0) {
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;

            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
        }
        return array;
    }

    function randomIntFromInterval(min, max) { // min and max included
        return Math.floor(Math.random() * (max - min + 1) + min);
    }

    function generateRoomName() {
        var charArray = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];
        var digitArray = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
        var roomName = shuffle(digitArray).join('').substring(0, randomIntFromInterval(3, 6)) + shuffle(charArray).join('').substring(0, randomIntFromInterval(7, 10));
        return shuffle(roomName.split('')).join('');
    }

    ext.point('io.ox/calendar/edit/section').extend({
        id: 'create-webconf',
        index: 310,
        draw: function () {
            this.append(
                $('<div class="hidden-xs col-sm-6 find-free-time">')
                    .append($('<button type="button" class="btn btn-link mb-4" data-action="create-webconf">')
                    .text('Créer un lien webconf')
                        .on('click', function (e) {
                            var url = 'https://webconf.numerique.gouv.fr/' + generateRoomName();
                            e.stopPropagation();
                            var container = this.closest('.window-container');

                            var target = container.querySelector('.form-control[name="location"]');

                            if (target) {
                                target.value = url;

                                var inputEvent = new Event('input', { bubbles: true });
                                target.dispatchEvent(inputEvent);

                                var changeEvent = new Event('change', { bubbles: true });
                                target.dispatchEvent(changeEvent);

                                setTimeout(function () {
                                    target.focus();
                                }, 50);
                            }
                        })
                    )
            );
        }
    });

    ext.point('io.ox/core/foldertree/calendar/links/subscribe').extend({
        index: 100,
        draw: function () {
            var folder = api.getDefaultFolder('calendar');
            // guests might have no default folder
            if (!folder) return;
            this.link('folder', gt('Créer un agenda'), function (e) {
                e.data = { folder: folder, module: 'event' };
                addFolder(e);
            });
        }
    });

    ext.point('io.ox/calendar/edit/section').extend({
        id: 'add-participant-ext',
        index: 950,
        rowClass: 'collapsed mb-4',
        draw: function (baton) {
            baton.parentView.addParticipantsView = new AddParticipantView({
                apiOptions: {
                    contacts: true,
                    users: true,
                    groups: true,
                    resources: true,
                    distributionlists: true
                },
                convertToAttendee: true,
                collection: baton.model.getAttendees(),
                blacklist: settings.get('participantBlacklist') || false,
                scrollIntoView: true,
                // to prevent addresspicker from processing data asynchronously.
                // Not needed and may cause issues with slow network (hitting save before requests return).
                processRaw: true,
                labelVisible: true,
                placeholder: gt('Name or email address') + ' ...'
            });

            var guid = _.uniqueId('form-control-label-');

            this.append(
                $('<label class="control-label col-xs-12">').attr('for', guid).append($.txt(gt('Participants, salles et ressources'))));
        }
    });
});
